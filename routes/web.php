<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route middleware untuk auth
Route::group(['middleware' => ['auth']], function () {
    //
    Route::get('/', 'HomeController@home');
    Route::get('/register', 'AuthController@register');
    Route::post('/welcome', 'AuthController@getRegister');

    Route::get('/data-tables', function () {
        return view('data-tables');
    });
    
    Route::get('/table', function () {
        return view('table');
    });
    // Route CRUD dengan Query Builder
    Route::get('/cast', 'CastController@index');
    Route::get('/cast/create', 'CastController@create');
    Route::post('/cast', 'CastController@store');
    Route::get('/cast/{cast_id}', 'CastController@show');
    Route::get('/cast/{cast_id}/edit', 'CastController@edit');
    Route::put('/cast/{cast_id}', 'CastController@update');
    Route::delete('/cast/{cast_id}', 'CastController@destroy');

    Route::resource('genre', 'GenreController');
    Route::resource('peran', 'PeranController');
});

// Route CRUD dengan resource ORM
Route::resource('film', 'FilmController');
Route::post('/film/{film_id}', 'FilmController@kritik');
Route::resource('profile', 'ProfileController')->only([
    'index', 'update'
]);

Auth::routes();




