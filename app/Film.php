<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    protected $table = 'film';
    protected $fillable = ['judul', 'ringkasan', 'tahun', 'poster', 'genre_id'];

    public function genre()
    {
        return $this->belongsTo('App\Genre'); // film belongsTo/dimiliki oleh 1 genre (mengarah ke model Genre)
    }

    public function peran()
    {
        return $this->hasMany('App\Peran'); // genre hasMany/memiliki banyak film (mengarah ke model Film)
    }

    public function kritik()
    {
        return $this->hasMany('App\Kritik'); // genre hasMany/memiliki banyak film (mengarah ke model Film)
    }
}
