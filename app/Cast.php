<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cast extends Model
{
    protected $table = 'cast';
    protected $fillable = ['nama', 'umur', 'bio'];

    public function peran()
    {
        return $this->hasMany('App\Peran'); // genre hasMany/memiliki banyak film (mengarah ke model Film)
    }
}
