<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profile';
    protected $fillable = ['umur', 'bio', 'alamat', 'user_id'];

    public function user(){
        //belongsTo disimpan di model child (yg memiliki foreign key)
        return $this->belongsTo('App\User'); // function ini belongsTo/mengarah/dimiliki model user
      }
}
