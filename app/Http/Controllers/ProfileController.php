<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use Auth;

class ProfileController extends Controller
{
    public function index()
    {
        $profile = Profile::where('user_id', Auth::user()->id)->first(); // dimana user_id adalah id dari user yang login (terauth), first karna 1 data yg di ambil
        return view('profile.index', compact('profile'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            // form
    		'umur' => 'required',
    		'bio' => 'required',
    		'alamat' => 'required'
        ]);

        $profile_data = [
            'umur' => $request->umur,
    		'bio' => $request->bio,
    		'alamat' => $request->alamat
        ];

        Profile::whereId($id)->update($profile_data);

        return redirect('/profile')->with('success', 'Berhasil Edit Profile!');

        // return redirect('/film')->with('success', 'Berhasil Edit film!');
    }
}
