<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Peran;
use App\Cast;
use App\Film;

class PeranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $peran = Peran::all();
        return view('peran.index', compact('peran'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // mengambil dari Model Film (tabel db film untuk mengambil id)
        $film = Film::all();
        $cast = Cast::all();
        return view('peran.create', compact('film', 'cast')); // compact untuk melempar value
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            // form
    		'film_id' => 'required',
    		'cast_id' => 'required',
    		'nama' => 'required'
    	]);
        
        //model
        Peran::create([
            // table field => form
    		'film_id' => $request->film_id,
    		'cast_id' => $request->cast_id,
    		'nama' => $request->nama
    	]);
        
        return redirect('/peran')->with('success', 'Berhasil tambah Peran!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
