<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Genre; //use model
use App\Film; // use model
use App\User; // use model
use App\Kritik; // use model
use Auth;
use File; //use fungsi File

class FilmController extends Controller
{
    public function __construct()
    {

        $this->middleware('auth')->except('index', 'show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $film = Film::all();
        return view('film.index', compact('film'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        // mengambil dari Model Genre (tabel db genre untuk mengambil genre.genre_id)
        $genre = Genre::all();
        return view('film.create', compact('genre')); // compact untuk melempar value
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    
        $this->validate($request,[
            // form
    		'judul' => 'required',
    		'ringkasan' => 'required',
    		'tahun' => 'required',
    		'poster' => 'required|mimes:jpeg,jpg,png|max:2200',
    		'genre_id' => 'required'
    	]);
        
        $gambar = $request->poster; // request file poster tampung di var gambar
        $new_gambar = time().' - '.$gambar->getClientOriginalName(); //ambil nama original file poster, gabung dengan waktu, tampung di new gambar

        //model
        Film::create([
            // table field => form
    		'judul' => $request->judul,
    		'ringkasan' => $request->ringkasan,
    		'tahun' => $request->tahun,
    		'poster' => $new_gambar, // yg disimpan di field poster hanya nama baru gambarnya
    		'genre_id' =>$request->genre_id
    	]);
        $gambar->move('img/', $new_gambar); // pindahkan gambar ke direktori img dengan nama gambar baru nya
        return redirect('/film')->with('success', 'Berhasil tambah film!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $film = Film::find($id);
        $kritik = Kritik::all();
        return view('film.show', compact('film', 'kritik'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $film = Film::find($id);
        $genre = Genre::all();
        return view('film.edit', compact('film', 'genre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            // form
    		'judul' => 'required',
    		'ringkasan' => 'required',
    		'tahun' => 'required',
    		'poster' => 'mimes:jpeg,jpg,png|max:2200',
    		'genre_id' => 'required'
    	]);

        $film = Film::findorfail($id);
        
        if ($request->has('poster')) { //jika request terdapat poster
            $path = 'img/'; // direktori path gambar
            File::delete($path . $film->poster); // maka delete file poster di path
            $gambar = $request->poster; // lalu request ulang poster baru tampung di var gambar
            $new_gambar = time().' - '.$gambar->getClientOriginalName(); //ambil nama original file, gabung dengan waktu, tampung di new gambar
            $gambar->move($path, $new_gambar); // lalu pindahkan gambar ke direktori img dengan nama gambar baru nya
            $film_data = [
                'judul' => $request->judul,
                'ringkasan' => $request->ringkasan,
                'tahun' => $request->tahun,
                'poster' => $new_gambar,
                'genre_id' =>$request->genre_id
            ];
        } else {
            $film_data = [
                'judul' => $request->judul,
                'ringkasan' => $request->ringkasan,
                'tahun' => $request->tahun,
                'genre_id' =>$request->genre_id
            ];
        }
        
        $film->update($film_data);

        return redirect('/film')->with('success', 'Berhasil Edit film!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $film = Film::findorfail($id); // cari film berdasarkan id
        $film->delete();

        $path = 'img/'; // direktori path gambar
        File::delete($path . $film->poster); // maka delete file poster di path
        return redirect('/film')->with('success', 'Berhasil Hapus film!');
    }

    public function kritik(Request $request, $id)
    {
        $film = Film::find($id);
        // $user = User::find($id);
        Kritik::create([
            // table field => form
    		'isi' => $request->isi,
    		'point' => $request->point,
    		'film_id' => $film->id, // 
    		'user_id' => Auth::user()->id
    	]);
        // return redirect('/film');
        return redirect()->action('FilmController@show', $id);
        // For a route with the following URI: profile/{id}

        // return redirect()->route('film', $id);

        // return redirect()->action(
        //     FilmController::class, 'show', ['id' => $id]
        // );
    }
}
