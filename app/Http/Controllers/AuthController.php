<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('register');
    }

    public function getRegister(Request $request)
    {
        // dd($request->all());
        $firstname = $request->fname;
        $lastname = $request->lname;

        return view('welcome', compact('firstname', 'lastname'));
    }
}
