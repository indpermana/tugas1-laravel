<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peran extends Model
{
    protected $table = 'peran';
    protected $fillable = ['film_id', 'cast_id', 'nama'];

    public function film()
    {
        return $this->belongsTo('App\Film'); // film belongsTo/dimiliki oleh 1 genre (mengarah ke model Genre)
    }

    public function cast()
    {
        return $this->belongsTo('App\Cast'); // film belongsTo/dimiliki oleh 1 genre (mengarah ke model Genre)
    }
}
