@extends('layout.master')
@section('judul')
    Register
@endsection

@section('content')
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <!-- Input Text -->
        <label for="fname">First name:</label><br><br>
        <input type="text" id="fname" name="fname" value=""><br><br>
        <label for="lname">Last name:</label><br><br>
        <input type="text" id="lname" name="lname" value=""><br><br>
        
        <!-- Input Radio Button -->
        <label>Gender:</label><br><br> 
        <input type="radio" id="male" name="gender" value="Male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="Female">
        <label for="female">Female</label><br>
        <input type="radio" id="other" name="gender" value="Other">
        <label for="other">Other</label><br><br>

        <!-- Elemen Select -->
        <label for="nationality">Nationality:</label><br><br>
        <select id="nationality" name="nationality">
            <option value="indonesian">Indonesian</option>
            <option value="singaporean">Singaporean</option>
            <option value="malaysian">Malaysian</option>
            <option value="australian">Australian</option>
        </select><br><br>
        
        <!-- Input Checkbox -->
        <label>Language Spoken:</label><br><br>
        <input type="checkbox" id="indonesia" name="language" value="Indonesia">
        <label for="indonesia">Bahasa Indonesia</label><br>
        <input type="checkbox" id="english" name="language" value="English">
        <label for="english">English</label><br>
        <input type="checkbox" id="other" name="language" value="Other">
        <label for="other">Other</label><br><br>
        
        <!-- Elemen TextArea -->
        <label>Bio:</label><br><br>
        <textarea name="message" rows="10" cols="30"></textarea><br>
        
        <!-- Input Submit -->
        <input type="submit" value="Sign Up">
      </form>
@endsection