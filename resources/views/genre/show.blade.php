@extends('layout.master')
@section('judul')
    Detail Genre - id : {{ $genre->id }}
@endsection

@section('content')

    <h3 class="text-primary">{{$genre->nama}}</h3>
    <p><a href="/genre">Kembali</a></p>

@endsection