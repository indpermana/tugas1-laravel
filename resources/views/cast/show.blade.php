@extends('layout.master')
@section('judul')
    Detail Cast - id : {{ $cast->id }}
@endsection

@section('content')

    <h3 class="text-primary">{{$cast->nama}}</h3>
    <p>{{$cast->umur}} tahun</p>
    <p>{{$cast->bio}}</p>
    <p><a href="/cast">Kembali</a></p>

@endsection