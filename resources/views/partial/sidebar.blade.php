<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/" class="brand-link">
      <img src="{{ asset('template/dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      @auth
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('template/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">                                                      
                                                                    {{-- profile = function profile di model User --}}
          <a href="#" class="d-block">{{ Auth::user()->name }} ({{ Auth::user()->profile->umur }})</a>
        </div>
      </div>
      @endauth

      @guest
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('template/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Guest</a>
        </div>
      </div>
      @endguest


      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          @auth           
          <li class="nav-item">
            <a href="/" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-table"></i>
              <p>
                Tables
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/table" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Table</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/data-tables" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>DataTable</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="/genre" class="nav-link">
              <i class="nav-icon fas fa-plus-square"></i>
              <p>
                Genre
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/cast" class="nav-link">
              <i class="nav-icon fas fa-plus-square"></i>
              <p>
                Cast
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/peran" class="nav-link">
              <i class="nav-icon fas fa-plus-square"></i>
              <p>
                Peran
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/profile" class="nav-link">
              <i class="nav-icon fas fa-cog"></i>
              <p>
                Profile
              </p>
            </a>
          </li>
          @endauth

          <li class="nav-item">
            <a href="/film" class="nav-link">
              <i class="nav-icon fas fa-plus-square"></i>
              <p>
                Film
              </p>
            </a>
          </li>

          @guest
          <li class="nav-item">
            <a href="/login" class="nav-link">
              <i class="nav-icon fas fa-plus-square"></i>
              <p>
                Login
              </p>
            </a>
          </li>
          @endguest

          @auth              
            <li class="nav-item bg-danger">
                  <a class="nav-link" href="{{ route('logout') }}"
                      onclick="event.preventDefault();
                                  document.getElementById('logout-form').submit();">
                      {{-- <i class="nav-icon fas fa-plus-square"></i> --}}
                      {{ __('Logout') }}
                  </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                  </form>
            </li>
          @endauth
          
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>