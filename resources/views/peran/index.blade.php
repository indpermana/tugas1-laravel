@extends('layout.master')
@section('judul')
    List Peran
@endsection

@section('content')

    <a href="/peran/create" class="btn btn-primary my-2">Tambah</a>
    <table class="table">
        <thead class="thead-light">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Film</th>
            <th scope="col">Cast</th>
            <th scope="col">Nama Peran</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
            @forelse ($peran as $key=>$value)
                <tr>
                    <td>{{$key + 1}}</th>
                    <td>{{$value->film->judul}}</td>
                    <td>{{$value->cast->nama}}</td>
                    <td>{{ $value->nama }}</td>
                    <td>
                        <form action="/peran/{{$value->id}}" method="POST">
                            <a href="/peran/{{$value->id}}" class="btn btn-info">Show</a>
                            <a href="/peran/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-danger my-1" value="Delete">
                        </form>
                    </td>
                </tr>
            @empty
                <tr colspan="5">
                    <td>No data</td>
                </tr>  
            @endforelse              
        </tbody>
    </table>

@endsection