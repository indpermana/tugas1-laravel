@extends('layout.master')
@section('judul')
    Tambah Peran
@endsection

@section('content')
    <form action="/peran" method="POST">
        @csrf
        <div class="form-group">
            <label for="film_id">Film</label>
            <select class="form-control" name="film_id" id="film_id">
                <option value="">-- Pilih Film -- </option>
                @foreach ($film as $item)
                    <option value="{{ $item->id }}">{{ $item->judul }}</option>
                @endforeach
            </select>
            @error('film_id')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="cast_id">Cast</label>
            <select class="form-control" name="cast_id" id="cast_id">
                <option value="">-- Pilih Cast -- </option>
                @foreach ($cast as $item)
                    <option value="{{ $item->id }}">{{ $item->nama }}</option>
                @endforeach
            </select>
            @error('cast_id')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan nama">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
@endsection