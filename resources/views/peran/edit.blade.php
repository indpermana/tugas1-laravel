@extends('layout.master')
@section('judul')
    Edit Peran
@endsection

@section('content')
    <form action="/peran/{{ $peran->id }}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="film_id">Film</label>
            <select class="form-control" name="film_id" id="film_id">
                <option value="">-- Pilih Film -- </option>                    
                
                @foreach ($film as $item)
                    @if ($item->id === $peran->film_id)
                        <option value="{{ $item->id }}" selected>{{ $item->judul }}</option>
                    @else
                        <option value="{{ $item->id }}">{{ $item->judul }}</option>
                    @endif
                @endforeach
            </select>
            @error('film_id')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="cast_id">Cast</label>
            <select class="form-control" name="cast_id" id="cast_id">
                <option value="">-- Pilih Cast -- </option>
                @foreach ($cast as $item)
                    @if ($item->id === $peran->cast_id)
                        <option value="{{ $item->id }}" selected>{{ $item->nama }}</option>
                    @else
                        <option value="{{ $item->id }}">{{ $item->nama }}</option>
                    @endif
                @endforeach
            </select>
            @error('cast_id')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" name="nama" id="nama" value="{{ $peran->nama }}" placeholder="Masukkan nama">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
@endsection