@extends('layout.master')
@section('judul')
    List Film
@endsection

@section('content')

@auth
    <a href="/film/create" class="btn btn-success my-2">Tambah Film</a>
@endauth
<div class="row">
    @foreach ($film as $item)    
    <!-- membuat card menjadi 3 colom (max 12/3 = col-4) -->
    <div class="col-4">
        <div class="card">
            <img src="{{ asset('img/'.$item->poster) }}" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">{{ $item->judul }} ({{ $item->tahun }}) <span class="badge badge-info">{{ $item->genre->nama }}</span></h5>
                <p class="card-text">{{ Str::limit($item->ringkasan, 100) }}</p>
                @auth
                    <form action="/film/{{ $item->id }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <a href="/film/{{ $item->id }}" class="btn btn-primary">Read More</a>
                        <a href="/film/{{ $item->id }}/edit" class="btn btn-info">Edit</a>
                        <input type="submit" class="btn btn-danger" value="Delete">
                    </form>
                @endauth
                    
                @guest
                    <a href="/film/{{ $item->id }}" class="btn btn-primary">Read More</a>
                @endguest
                <small>{{ $item->created_at }}</small>
            </div>
        </div>
    </div>
    @endforeach
</div>


@endsection