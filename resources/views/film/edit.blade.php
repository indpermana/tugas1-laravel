@extends('layout.master')
@section('judul')
    Edit Film - {{ $film->judul }}
@endsection

@section('content')
    <form action="/film/{{ $film->id }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="judul">Judul</label>
            <input type="text" class="form-control" name="judul" id="judul" value="{{ $film->judul }}" placeholder="Masukkan judul">
            @error('judul')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="ringkasan">Ringkasan</label>
            <textarea class="form-control" name="ringkasan" id="ringkasan" cols="30" rows="10" placeholder="Masukkan ringkasan">{{ $film->ringkasan }}</textarea>
            @error('ringkasan')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="genre_id">Genre</label>
            <select class="form-control" name="genre_id" id="genre_id">
                <option value="">-- Pilih Genre --</option>
                @foreach ($genre as $item)
                    @if ($item->id === $film->genre_id)
                        <option value="{{ $item->id }}" selected>{{ $item->nama }}</option>
                    @else
                        <option value="{{ $item->id }}">{{ $item->nama }}</option>
                    @endif
                @endforeach
            </select>
            @error('genre_id')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="poster">Poster</label>
            <input type="file" class="form-control" name="poster" id="poster">
            @error('poster')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="tahun">Tahun</label>
            <input type="number" class="form-control" name="tahun" id="tahun" value="{{ $film->tahun }}" placeholder="Masukkan tahun">
            @error('tahun')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
@endsection