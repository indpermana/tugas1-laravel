@extends('layout.master')
@section('judul')
    Detail Film - {{ $film->judul }}
@endsection

@section('content')
    <div class="row">
        <div class="col">
            <div class="row">
                <div class="col">
                    <img src="{{ asset('img/'.$film->poster) }}" width="50%" alt="...">
                </div>
                <div>
                    <h4>List Cast</h4>
                    <ul>
                        @foreach ($film->peran as $item)
                            {{-- nama cast, karena looping sudah masuk ke method peran(akses model peran)
                                maka bisa di ambil fungsi cast di model peran
                                                           nama peran --}}
                            <li>{{ $item->cast->nama }} - {{ $item->nama }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <h3>{{ $film->judul }} ({{ $film->tahun }})</h3>
            <p>{{ $film->ringkasan }}</p>
            <small>{{ $film->created_at }}</small>
            <br>
            <a href="/film" class="btn btn-primary">Kembali</a>
            <a href="{{ asset('img/'.$film->poster) }}" target="_blank" class="btn btn-info">
                <i class="fa fa-download"></i> Download File
            </a>
        </div>
    </div>

    {{-- comments form --}}
    <div class="card my-4">
        <h5 class="card-header bg-light text-dark">Leave a Comment</h5>
        <div class="card-body">
            <form action="/film/{{ $film->id }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="point">Rating</label>
                    <input type="number" class="form-control" name="point" id="point" placeholder="Masukkan rating" min="1" max="5">
                    @error('point')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="isi">Kritik</label>
                    <textarea class="form-control" name="isi" id="isi" cols="30" rows="10" placeholder="Masukkan isi"></textarea>
                    @error('isi')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Tambah</button>
                
                @guest
                    <p><i>*Login terlebih dahulu untuk dapat berkomentar </i></p>
                @endguest
            </form>
        </div>    
    </div>

    {{-- single comment --}}
    @foreach ($kritik as $item)
    @if ($item->film_id==$film->id)
        <div class="media mb-4 bg-light">
            <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="...">
            <div class="media-body">
                <h5 class="mt-0 text-primary" >{{ $item->user->name}}</h5>
                <small>Rating : {{ $item->point }}</small>
                <p>{{ $item->isi }}</p>
            </div>
        </div>
    @endif
        
    @endforeach
    
    
        
            
        
    

@endsection