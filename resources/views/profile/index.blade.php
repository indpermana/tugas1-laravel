@extends('layout.master')
@section('judul')
    Halaman update Profile
@endsection

@section('content')

<form action="/profile/{{$profile->id}}" method="POST">
    @csrf
    @method('PUT')
    {{-- menampilkan email dan user yang sedang login --}}
    <div class="form-group">
        <label for="email">Email</label>
                                                {{-- user = function model, email=column model --}}
        <input type="email" class="form-control" value="{{$profile->user->email}}" id="email" disabled>        
    </div>
    <div class="form-group">
        <label for="name">Name</label>
                                                {{-- user = function model, email=column model --}}
        <input type="text" class="form-control" value="{{$profile->user->name}}" id="name" disabled>        
    </div>

    {{-- end --}}

    <div class="form-group">
        <label for="umur">Umur</label>
        <input type="number" class="form-control" name="umur" value="{{$profile->umur}}" id="umur" placeholder="Masukkan Umur">
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    
    <div class="form-group">
        <label for="bio">Bio</label>
        <textarea class="form-control" name="bio" id="bio" cols="30" rows="10" placeholder="Masukkan Bio">{{$profile->bio}}</textarea>
        @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label for="alamat">Alamat</label>
        <textarea class="form-control" name="alamat" id="alamat" cols="30" rows="10" placeholder="Masukkan Alamat">{{$profile->alamat}}</textarea>
        @error('alamat')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Edit</button>
</form>

@endsection